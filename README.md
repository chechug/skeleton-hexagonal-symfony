# Skeleton Hexagonal Symfony
Basic Skeleton Symfony 4 with Hexagonal Arquitecture, you can start your project with this base.


## Changing namespaces
- To change namespace you need to edit composer.json, to do this, search "autoload" lines:

    ```
    "autoload": {
        "psr-4": {
            "YourNamespaceHere\\": "src/{Context}"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "YourNamespaceHere\\Tests\\": "tests/{Context}"
        }
    ```
- src/Kernel.php (Just change namespace to your new namespace)

- public/index.php  (Change namespace kernel to new namespace)
 
- bin/console (Change namespace kernel to new namespace)


---

##

- composer dump-autoload
- composer update

---

## Starting with Hexagonal Arquitecture on Symfony
**Controller :** 

-  config/services.yaml
    ```
    {namespace}}\Infrastructure\UserInterface\Controller\:
        resource: '../src/Infrastructure/UserInterface/Controller'
        tags: ['controller.service_arguments']
    ```
			
			
	
Before made a push we need to see we have a .gitignore well set up to not upload a personal file or unnecessary. This is a basic example of gitignore

## .gitignore File
```
###> symfony/framework-bundle ###

/.env
/public/bundles/
/var/
/vendor/

###< symfony/framework-bundle ###


###> symfony/webpack-encore-pack ###
/node_modules/
/public/build/
npm-debug.log
yarn-error.log
###< symfony/webpack-encore-pack ###


###<> symfony/phpunit-bridge ###
.phpunit
/phpunit.xml
###<< symfony/phpunit-bridge ####


###<> symfony/web-server-bundle ###
/.web-server-pid
###<< symfony/web-server-bundle ###


###:

/.idea
/.directory
/composer.lock
/symfony.lock
/.gitignore
```